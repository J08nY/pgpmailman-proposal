# Milestones

## Week 1

 * Create the encrypted lists plugin and app repos (`pgpmailman` and `django-pgpmailman`) on gitlab.
 * Have a real-time meeting with mentors, voice possible? Ring.cx / IRC?
 * Finalize core changes and plugin design.
 * **Check:** Design specs for ^^ are pushed to [repo](https://gitlab.com/J08nY/pgpmailman-proposal).

## Week 2

 * Designate which core changes have priority and are necessary for the plugin.
 * Implement core changes necessary for the plugin.
 * Have a meeting discussing the MRs and comments.
 * **Check:** Merge requests for changes planned in the week submitted, with pipelines succeeding.

## Week 3

 * Implement `styles`, 

## Week 4

 * 

## First Evaluation 26.6-30.6

## Week 5

## Week 6

## Week 7

## Week 8

## Second Evaluations 24.7-28.7

## Week 9

## Week 10

## Week 11

## Week 12

## Final Evaluations 21.8-29.8




