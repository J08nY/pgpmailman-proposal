# PGPMailman-proposal

A repository of design documentation for the 2017 GSoC project: Mailman - Encrypted mailing lists.

 * See [core_changes.md](/core_changes.md) for changes proposed to various parts of Mailman in order to make an encrypted lists plugin and other plugins possible.
 * See [original.md](/original.md)/[original.pdf](/original.pdf) for the original (selected) proposal.
 * See [plugin.md](/plugin.md) for the current working version of the proposal.
 * See [milestones.md](/milestones.md) for milestones planned for the Coding period.