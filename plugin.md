# Mailman PGP plugin

## Structure

  * `mailman_pgp` - A Core plugin.
     - `styles` - The plugin is enabled for a list by creating it with one of the plugin provided list styles. Which mirror the core list styles `legacy-default` for discussion style and `legacy-announce` for an announce style.
        - `PGPDiscussionStyle`
        - `PGPAnnounceStyle`

     - `pgp` - Package that handles lower-level pgp related stuff. Such as handling of PGP encrypted/containing messages via message wrappers, key generation, and PGP configuration.
        - `InlineWrapper` - Tries to handle messages with inline PGP.
        - `MIMEWrapper` - Handles PGP/MIME messages as per RFC 3156.
        - `PGPWrapper` - Combines both inline and MIME wrappers for utility.
        - `ListKeyGenerator` - A `multiprocessing` process that generates the list key out of process and stores it in the list key directory.
        - `PGP`- Instance attached to the `config` as `config.pgp`. Provides access to the PGP related configuration.

     - `commands`
        - `KeyEmailCommand` - Handles user key management through the `key <subcommand> [args]` command.
            - `set <token>`
            - `confirm <token>`
            - `change`
            - `receive`
            - `sign`

     - `config` - For plugin specific configuration and parsing.
        - `Config(ConfigParser)` - An instance of this class is a used as a module global `config`.

     - `database` - SQLAlchemy utilities.
        - `Database` - Instance attached to the `config` as `config.db`.

     - `model`
        - `PGPMailingList` - Stores plugin specific mailing list metadata and configuration for a PGP enabled mailing list. It is created by the plugin list styles.
            - `unsigned_msg_action` = Column(Enum(Action), default=Action.reject)
            - `inline_pgp_action` = Column(Enum(Action), default=Action.defer)
            - `expired_sig_action` = Column(Enum(Action), default=Action.reject)
            - `revoked_sig_action` = Column(Enum(Action), default=Action.reject)
            - `invalid_sig_action` = Column(Enum(Action), default=Action.reject)
            - `duplicate_sig_action` = Column(Enum(Action), default=Action.reject)
            - `strip_original_sig` = Column(Boolean, default=False)
            - `sign_outgoing` = Column(Boolean, default=False)
            - `nonencrypted_msg_action` = Column(Enum(Action), default=Action.reject)
            - `encrypt_outgoing` = Column(Boolean, default=True)
        - `PGPAddress` - Stores plugin specific metadata of an address. Stores the address key fingerprint and whether it was confirmed. The address public key is stored in a configurable key directory and looked up by the key fingerprint. Is created during the first subscription of a given address to a PGP enabled mailing list in the respective `SubscriptionWorkflow`.
        - `PGPSigHash` - Stores a pair, signature digest and key fingerprint from the signature. Used to stop replay attacks. Every successful posting to a PGP enabled mailing list is checked for signatures and their hashes are entered into this db.

     - `mta`
        - `PGPBulkDelivery` - A PGP enabled bulk delivery, throws user keyids to preserve anonymity.
        - `PGPIndividualDelivery` - A PGP enabled individual delivery.
        - `deliver` - The custom `outgoing` callable which chooses the appropriate PGP delivery classes for a PGP enabled mailing list.

     - `rest` - Module that exposes plugin-specific REST api, which will be used by the `django-pgpmailman` app. With the `/plugins/pgp/` root.
        - `/lists/<list-id>` - For setting per-list configuration options: bounce/discard non-signed, bounce/discard non-encrypted.
        - `/lists/<list-id>/key` - For getting the list-key.
        - `/users/<user-id>/key` - For getting, setting and revoking the user-key.

     - `runners`
        - `PGPIncomingRunner` - Decrypts incoming messages for encrypted mailing lists before passing them to the default IncomingRunner. Messages to ordinary lists are passed to the default IncomingRunner without change. Additionally if it needs to moderate a message, it sets up the moderation data for the `PGPEncryptionRule` which enforces it.

     - `rules`
        - `PGPEncryptionRule` - Checks for moderation data from the incoming runner which processed the message, if found, matches (jumps to moderation chain).
        - `PGPSignatureRule` - Checks the message for PGP signatures, and enforces list policy.

     - `chains`
        - `PGPChain` - A PGP enabled posting chain that is set by a PGP enabled list style. Contains the `PGPEncryptionRule`, `PGPSignatureRule` and then passes to the `default-posting-chain`.
 
     - `archivers`
        - `PGPRemoteArchiver` - Fetches list archive public keys from `django-pgpmailman`, uses them to send messages to archive encrypted, for encrypted lists.
        - `PGPLocalArchiver` - Stores messages encrypted locally, format TBD.

     - `workflows`
        - `SubscriptionWorkflows` - Several subscription policies, mirroring the core subsription policies, with added steps for obtaining and confirming the users public key via the `key set` and `key confirm` commands.
        - `KeyChangeWorkflow` - A workflow used to store the state of the `key change` command.

     - `plugin`
        - `PGPMailman` - Main plugin class, has `pre_hook`, `post_hook` and `rest_object` methods necessary for the IPlugin interface it implements.



  * `django-pgpmailman` - A Django app that extends Postorius and HyperKitty, also with an associated example_project that combines Postorius, HyperKitty while extending their templates and views seamlessly.
     - `api`
        - `/list/<list-id>/key` - For getting the list-archive public key.
     - `templates` - Will have custom templates as well as templates overwriting and extending certain Postorius and HyperKitty templates.


## Instalation

### Core plugin

A setuptools package that needs to be installed into the same virtualenv as the Mailman 3 instance.

### Django app

A setuptools package that needs to be installed into the same virtualenv as the Postorius and HyperKitty instances, packaged with an example_project.


## Configuration

### Core plugin

Custom runners, archivers and other site configuration options would need to be set.

### Django app

As any of Mailman's Django apps, will be packaged with an example_project with default configuration.


## Documentation

In order for site admins and users to use encrypted mailing lists responsibly, quite some amount of documentation is required, since misusing encrypted lists will make them ineffective.
