## Proposed changes to Mailman core

  1. Add configuration option similar to `config.styles.paths` but for general plugins. Paths in this list will be used to find components that Mailman will use, similar to `find_components` coupled with a standardized plugin structure:
    - `plugin package`
        - `handlers`
        - `rules`
        - `chains`
        - `commands`
        - `pipelines`
        - `styles`
        - `rest`
    - [MR !288](https://gitlab.com/mailman/mailman/merge_requests/288)
      Adds a whole configuration section for plugins, where each plugin has the following options:
        - name of the section
        - `class:` a class implementing the new `IPlugin` interface.
        - `path:` a python package available on `sys.path` under which the aforementioned plugin structure, searched for components, resides.
        - `enable:` whether the plugin is enabled, only enabled plugins are searched for components, their classes instantiated, their hooks run and their REST routes exposed.
        - `configuration:` optional configuration path, which the plugin can use to read/write it's configuration file.

  2. Let plugins add Pipelines the same way they can add Handlers, Rules etc...
    - [<s>MR !287</s> merged](https://gitlab.com/mailman/mailman/merge_requests/287)
    - This means refactoring `BasePipeline`, `OwnerPipeline`, `PostingPipeline`, `VirginPipeline` from `mailman.core.pipelines.py` into a package `mailman.pipelines`
    - Use `find_components`, or rather it's sibling function created in 1.

  3. Let plugins subscribe to receive events.
    - [MR !288](https://gitlab.com/mailman/mailman/merge_requests/288)

  4. Allow multiple callables in `pre_hook` and `post_hook` run in order specified. Also allow callables for when Mailman exits.
    - [MR !288](https://gitlab.com/mailman/mailman/merge_requests/288)
    - Already a similar [MR !264](https://gitlab.com/mailman/mailman/merge_requests/264).

  5. Allow Mailman core to send events to Postorius, HyperKitty and any app that subscribes through a new REST api call with a callback.
    - Implemented in the [mailman-rest-events](https://gitlab.com/J08nY/mailman-rest-events) plugin.

  6. Drop `ext_dir`.
    - [MR !288](https://gitlab.com/mailman/mailman/merge_requests/288)

  7. Let plugins add routes to the REST API. Under `/plugins/<plugin_name>/...`.
    - [MR !288](https://gitlab.com/mailman/mailman/merge_requests/288)
    - Use the current `ObjectRouter` with plugins providing a REST object.

  8. Add per-plugin config.
    - [MR !288](https://gitlab.com/mailman/mailman/merge_requests/288)

  9. Add `description` attribute to `IStyle` and the default styles, so that mailing list styles have some human readable string to show.
    - [MR !289](https://gitlab.com/mailman/mailman/merge_requests/289)

 10. Refactor Subscription and Unsubscription workflow to allow a plugin to add steps to it's state machine.
    - Introduces `IWorkflow` interface for the workflow state machine.
    - Allows the `WorkflowStateManager` to store more workflow steps than the current one and adds a migration for this change.
    - Refactors the monolithic SubscriptionWorkflow and UnsubscriptionWorkflow classes to reusable mixins, allowing to deduplicate their code and create custom workflows.
    - Dynamically loads all `IWorkflow` classes from Mailman core and plugins.
    - Migrates the `subscription_policy` and `unsubscription_policy` attributes from the `SubscriptionPolicy` enum, to store the name of the respective SubscriptionWorkflow class, dynamically loaded.
    - [MR !299](https://gitlab.com/mailman/mailman/merge_requests/299)
    - On top of [MR !300](https://gitlab.com/mailman/mailman/merge_requests/300) which instantiates components only in the `add_components` utility function and not at lower levels, since Workflow components need to be stored as classes and instantiated on use.

  11. Add SMTPS and STARTTLS support.
    - Adds SMTP over SSL/TLS and STARTTLS SMTP extension support to the `BaseDelivery` class via specialized `Connection` classes.
    - Creates a new `ISMTPConnection` interface, which `SMTPConnection`,`SMTPSConnection` and `STARTTLSConnection` implement.
    - [MR !286](https://gitlab.com/mailman/mailman/merge_requests/286)

## Proposed changes to MailmanClient

 1. Add bindings for specifying list-style on list creation.
    - [MR !28](https://gitlab.com/mailman/mailmanclient/merge_requests/28)

 2. Add bindings for `/plugins/<plugin_name>/...`.
    - Pretty low-level, since each plugin can handle requests as it wants.
    - [MR !34](https://gitlab.com/mailman/mailmanclient/merge-requests/34)

## Proposed changes to Postorius and HyperKitty

 1. Add additional blocks to some base templates.

 2. Add a dropdown list-style selection field to list creation template + view.
    - [MR !214](https://gitlab.com/mailman/postorius/merge_requests/214)

 3. Let Postorius and HyperKitty subscribe to receive events from Mailman Core, add an API route to receive them in django-mailman3.