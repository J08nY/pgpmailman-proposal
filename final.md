# Final work submission

  - Differs from [original proposal](https://gitlab.com/J08nY/pgpmailman-proposal/blob/master/original.pdf)
    - Done as a plugin for Mailman Core, instead of in-tree. [ref](https://mail.python.org/pipermail/mailman-developers/2017-March/026023.html)


## Mailman-pgp

  - [repository@gitlab](https://gitlab.com/J08nY/mailman-pgp)
  - [docs@rtd](https://mailman-pgp.readthedocs.io/en/latest/)
  - Plugin for Mailman Core.
  - Enables creating a PGP mailing list, which has a list key, can receive and serve messages encrypted, can sign and receive signed messages from subscribers.
  - Creates the `key` email command, which is used for per-address user key management.
  - Subscription to a PGP enabled mailing list the subscribing address to send and confirm an address public key, which the moderator must verify.
  - Somewhat confirms the user has possession of the appropriate private key to the one sent on subscription.
  - Has per-list settings for encryption/signatures/what to do with non encrypted /non signed messages, etc..
  - Optionally exposes a REST API for list configuration.
  - Has local archivers which can store the messages encrypted by the list key.
  - Stores list and address keys in configurable key directories.
  - Requires (some not merged) MRs in Mailman Core:
      - [mailman/pluggable-components](https://gitlab.com/mailman/mailman/merge_requests/288), merged as [!308](https://gitlab.com/mailman/mailman/merge_requests/308)
      - [mailman/list-style-descriptions](https://gitlab.com/mailman/mailman/merge_requests/289)
      - [mailman/pluggable-workflows](https://gitlab.com/mailman/mailman/merge_requests/299)
      - [mailman/pipelines-dynamical-instantiation](https://gitlab.com/mailman/mailman/merge_requests/287), merged
  - Additional MR (not required):
      - [mailman/mta-smtps-starttls](https://gitlab.com/mailman/mailman/merge_requests/286)
  - Required branches are merged and maintained at [J08nY/mailman/plugin](https://gitlab.com/J08nY/mailman/tree/plugin).
  - To install, do `pip install mailman-pgp`, warning: it will pull in a development version of [Mailman Core](https://gitlab.com/J08nY/mailman/tree/plugin) and [PGPy](https://github.com/SecurityInnovation/PGPy).


## django-pgpmailman

  - [repository@gitlab](https://gitlab.com/J08nY/django-pgpmailman)
  - A Django app, uses django-mailman3 and mailmanclient, integrates well with Postorius and HyperKitty.
  - Provides management of PGP enabled mailing lists to the list owner, and of PGP related subscription settings to the subscriber.
  - Requires (currently not merged) MRs in mailmanclient, django-mailman3, Postorius and HyperKitty:
      - [mailmanclient/plugin-bindings](https://gitlab.com/mailman/mailmanclient/merge_requests/34)
      - [django-mailman3/template-chunks](https://gitlab.com/mailman/django-mailman3/merge_requests/8)
      - [postorius/template-chunks](https://gitlab.com/mailman/postorius/merge_requests/215)
      - [hyperkitty/template-chunks](https://gitlab.com/mailman/hyperkitty/merge_requests/65)
  - Some screenshots:
![](https://neuromancer.sk/static/mailman_pgp_list_index.png)
![](https://neuromancer.sk/static/mailman_pgp_address_list.png)
![](https://neuromancer.sk/static/mailman_pgp_list_key_management.png)
![](https://neuromancer.sk/static/mailman_pgp_list_encryption.png)
![](https://neuromancer.sk/static/mailman_pgp_list_signature.png)


## mailman-rest-events

  - [repository@gitlab](https://gitlab.com/J08nY/mailman-rest-events)
  - A plugin for Mailman Core that turned out to be unnecessary for the working of django-pgpmailman, but implemented a similar feature as this [MR](https://gitlab.com/mailman/mailman/merge_requests/264).
  - This plugin sends the events (and some information about them) from Mailman Core to a list of configurable endpoints using JSON in HTTP POST requests.


## Other contributions

  - [mailmanclient/split-sources](https://gitlab.com/mailman/mailmanclient/merge_requests/38), merged
  - [postorius/list-style-selection](https://gitlab.com/mailman/postorius/merge_requests/214)
  - [postorius/fix/middleware-exception](https://gitlab.com/mailman/postorius/merge_requests/218)
  - [postorius/fix/mailmanclient-split](https://gitlab.com/mailman/postorius/merge_requests/219)
  - [django-mailman3/fix/middleware-exception](https://gitlab.com/mailman/django-mailman3/merge_requests/9)
  - Many many PRs to [PGPy](https://github.com/SecurityInnovation/PGPy), a Python only implementation of OpenPGP. [19 PRs](https://github.com/SecurityInnovation/PGPy/pulls?utf8=%E2%9C%93&q=is%3Apr%20author%3AJ08nY) and counting. As PGPy was not and still is not feature complete in regards to RFC4880 I found out many times that it's missing features/bugs broke mailman-pgp CI. It would not make sense fixing them locally, both from a software design perspective and open source software one aswell.