# Mailman - encrypted lists

This proposal aims to design and implement support for encrypted mailing lists into GNU Mailman using PGP/MIME and GNUPG.

## Benefits
Email is generally sent unencrypted (apart from TLS/SSL). It can pass through many potentially compromised or malicious servers. Users with PGP can use it to encrypt one-to-one messages or even group messages, provided they have a web-of-trust. However, encrypting mailing list conversations this way, without the mailing list server support, would be very tedious, if not impossible for most cases. This proposal adds support for encrypted mailing lists to one of the most widely used mailing list servers, GNU Mailman.

## Threat model

### Assets

  - Message body
  - Message metadata (headers + existence)
  - Subscriber's identity (list of subscribers of a given list + their public keys as described in this proposal)
    - Existence of subscription for a given address
    - Existence of subscription for a given key
  - Keyrings (and keypairs in them, as described in this proposal)

### Adversary

We assume that an adversary:

  - Can read, write, alter, drop any data passed between:
    - Mailman Core and it's outgoing and incoming MTA
    - outgoing and incoming MTA and lists subscribers
    - HyperKitty and Mailman Core (mailman-hyperkitty)
  - Can gain filesystem access to HyperKitty's archive
  - Is not a subscriber of the list / can not subscribe to the list, as it it's moderated
  - Can not gain access into a subscriber's mailbox as well as his private part of a PGP key *(Endpoint security out of scope)*
  - Can not gain physical access to the machine running Mailman Core or HyperKitty. *(RAM access, Coldboot mitigation out of scope)*
  - Can not get access to the machine running Mailman Core as the Mailman core user or root

Optional assumptions, these can be somewhat protected against and thus become real assumptions, see [Attacks and Mitigations](#attacks-and-mitigations):

  - Can get filesystem access with enough permissions to access (read/write) Mailman Core queues with Mailman offline
  - Can get filesystem access to keyrings described in this proposal

## Design

  - On top of PGP/MIME
  - Uses GNUPG keyrings

### List
  - has a list keypair stored in **core keyring**
  - has a list archive keypair, which is stored in **archive keyring**
  - public key from lists archive keypair also stored in  **core keyring**

### User workflow

#### List key
  - User gains knowledge of lists public key:
    - Through Postorious
    - Sends mail to `list_id`-key@domain.tld, receives the lists public key, signed by users that chose to publicly sign it. <u>TODO:</u> not the best solution, the problem of binding the list key to a list, and in general key management, is key for this project *(see what I did there? :)*

#### Subscription
  - Public key a required argument on list subscription, confirmation token sent encrypted with given key to subscribed address, signed confirmation token required (binds users public key with email address used for subscription)
  - List owner has to have a way to verify that the public key presented as well as the address which subscription was requested is valid and that it is the correct pair (address, pubkey)

#### List moderation
  - Subscription moderation required for an encrypted list, otherwise, what's the point?

#### Commands
  - Required to be signed by user's private key, otherwise discard/bounce (configurable), confirmation with a signed confirmation token required for every command, essentially all commands will need to have a workflow attached to them as `subscribe` has (to protect against replay attacks)
  - New command for key management
     - `key`
        - `change` - would require signature with old key, also a new key as an argument
        - `revoke` - would require a key revocation certificate, it would redo the challenge response confirmation to set the users key (essentially re-subscription)
        - `sign` - would require one argument, list's public key signed with users private key, this list key with users signature will be distributed as the list public key (if the signature is valid and from the correct subscriber of the list). Users who do this have to understand that their signature of the lists public key will be public, thus their subscription will also be public. <u>TODO:</u> It may be possible to allow non-subscribers to sign the lists public key, thus subscibers get some deniability of being a subscriber.

#### Posting
  - Based on list configuration, posting should be encrypted with list pubkey and signed with users privkey

#### Unsubscription
  - Same as now, signature required as for other commands

### Technical details

  - Will use OpenPGP at a low-level (OpenPGP packet manipulation) to:
    - keep the original user's signature
    - re-encrypt the message to the list's recipients (in configurable size batches)
    - re-encrypt in a way that strips key-ids (not to compromise privacy)
    - optionally strip the sender's signature
    - additionally sign with the list's private key
    - currently most promising python lib -> [py-pgp](https://github.com/mitchellrj/python-pgp) *(GPL v3)*
    - All possible, see [RFC 4880](https://tools.ietf.org/html/rfc4880)
  - Considered [SELS](http://sels.ncsa.illinois.edu/) or similiar proxy encryption scheme (see references), however:
    - In SELS the list server has no access to the message plaintext, which is great for confidentiality but not for list archiving / moderation / many Mailman features.
    - In SELS the list server generates the users private key or at least has to have some information derived from subscribers private keys to work.
    - In SELS, many tasks are offset from the list server to the list manager, which actually enables the level of confidentiality the list server has in that model, however seems very impractical.
    - General conclusion: SELS doesn't satisfy Mailman's needs for this project idea

### What and where?

  - Mailman Core
    - **core keyring** - (gnupg keyring) contains list keypairs, and list archive public keys
    - **users keyring** - (gnupg keyring) contains users public keys *(for all lists in a given Mailman instance)*
    - rules in chain, will enforce list configuration such as:
        - discard/bounce non-encrypted
        - discard/bounce non-signed
    - handlers
        - to strip additional header data leaks
        - to strip the original signature (if configured for anonymous lists)
    - runners
        - incoming runner needs to decrypt the message if its PGP/MIME encrypted since rules generally require access to the plaintext body of the message and separate the signature into `msgdata`
        - outgoing runner needs to attach the signature to the message, add lists signature and reencrypt for users
        - possibly a new runner to send dummy messages to subscribers to mitigate traffic analysis
    - REST API
        - list key management
        - user key management
        - <u>TODO:</u> this gives the BASIC AUTH required to access the REST api pretty huge permissions, a more granular access control would be beneficial
    - mta
        - SMTPS/STARTLS support (Already started working on this, will be ready for a MR after writing tests [need to change up the SMTPLayer test layer a bit])
    - models/db
        - address <-> key fingerprint (to find key in **users keyring**)
        - list <-> list key fingerprint
        - list <-> list archive key fingerprint
    - commands
        - `key` command for key management
    - new module -> security
        - provides interface to manage the **core** and **users keyrings** to rest of Mailman Core

  - Mailman client
    - binding of
        - list key management REST API
        - user key management REST API

  - HyperKitty
    - **archive keyring** - (gnupg keyring) contains list archive keypairs
    - decrypts messages received from Mailman-HyperKitty using list archive private keys
    - provides API to get list archive public key from the **archive keyring**
    - stores messages as received, encrypted by list archive key, decrypt when serving to subscribed users?
    - docs
        - strongly advise running HyperKitty behind https

  - Mailman-HyperKitty
    - has access to the **core keyring** with list archive public keys, uses them to encrypt before sending to HyperKitty

  - Postorius
    - list public key displayed
    - list configuration
        - list key management (possibly too dangerous if not run behind HTTPS, and even then), only accessible to list owners
    - list subscription
        - public key a required argument
    - user key management / user account management
        - all of user's actions for an address that is subscribed to an encrypted list will generate a confirmation token/text that will need to be:
            - signed by subscriber's key and pasted into Postorious
            - signed by subscriber's key and sent to `list-id`-request@domain.tld
    - docs
        - strongly advise running Postorious behind https

## Performance

  - Message encryption an obvious bottle-neck
  - Working with OpenPGP packets brings a speedup since the message itself is encrypted only once for a batch, but many PKESK packets will need to be created (this is unavoidable).
  - Current Mailman Core architecture with runners being separate processes adapts to this nicely

## Attacks and mitigations

  - User -> MTA
    - sniffing:
        - Messages encrypted with list pubkey
    - dropping:
        - No mitigation
    - writing, altering:
        - List configured to require subscribers signature

  - MTA -> Mailman
    - sniffing, altering, dropping, writing:
        - same as above

  - Mailman -> MTA
    - sniffing:
        - Messages encrypted with users public keys
    - dropping:
        - No mitigation
    - writing, altering:
        - Messages signed with list private key (in addition to any user's original signature)

  - MTA -> User
    - sniffing:
        - Messages encrypted with users pubkey
    - dropping:
        - No mitigation
    - writing, altering:
        - Messages signed with list private key (in addition to any user's original signature)

  - Replay attacks
    - Since user signature is kept, when the list is set to discard non-signed messages a replay attack without list subscribers noticing is not possible (as the signature couldn't be stripped). The signature of the original and replayed message would be the same, which would alert the subscribers that the message was replayed.

  - list <-> list pubkey binding
    - list key signed with list owners key, from that users that trust the list owner might also sign it and hopefully reach nice web-of-trust coverage to verify the list key
    - key also available on Postorious
    - <u>TODO:</u> very important, have some ideas

  - filesystem access:
     - Mailman Core queues (Mailman offline)
        - Put Mailman `queue_dir` or possibly the whole `var_dir` into an encrypted fs, mount on start (admin enters passphrase), unmount on quit
     - Mailman core keyring/HyperKitty archive keyring
        - Use a passphrase encrypted keyring, enter passphrase manually on Mailman start, use gpg-agent. <u>TODO:</u> gpg-agent doesn't have infinite ttl support, try merge upstream?

## Deliverables

  - Mailman Core
    - Working encrypted lists implementation, that follows the design above
    - Accepts PGP/MIME encrypted/signed incoming mail, does checks as specified by requirements and config
    - Processes the message as usual
    - Sends the message to HyperKitty encrypted
    - Sends the message to users PGP/MIME encrypted + signed
    - Provides REST API access to list and user key management
  - Postorious
    - Provides list and user key management features
    - Commands confirmed by signing a confirmation token
  - HyperKitty
    - Provides API to get list archive public key
    - Receives messages encrypted with list archive public key
    - Stores them encrypted

### Stretch/Optional

  - Mitigate the attacks with the stronger filesystem access assumptions by either creating docs on how to setup such a Mailman instance or add some mechanism so that Mailman does it itself.

## Timeline

  - Before May 30
    - Setup full working dev env (I currently have everything except working Postfix server)
    - Study Mailman sources, refine and collaborate on this proposal and design of encrypted lists, produce a more concrete design spec
    - Finish implementing SMTPS/STARTLS support and merge to Mailman Core
  - May 30 - June 26/30
    - Implement security module, to abstract working with keyrings, make changes to models to reflect the existence of encrypted lists, potentially create an encrypted list style?
    - Make Incoming runner decrypt a PGP/MIME encrypted message to an encrypted list
    - Add rules to chain that enforce encrypted lists configuration (discard/bounce non-encrypted etc.)
    - Modify commands to work with encrypted lists and require signed confirmation / additional arguments
    - Make Outgoing runner sign and encrypt to users
  - June 26/30 - July 24/28
    - Expose list and user key management to the REST API
    - Add bindings to the new API functions to mailman-client
    - Add list and user key management to Postorious
  - July 24/28 - August 21/29
    - Add keyring handling to HyperKitty
    - Expose list archive public key from HyperKitty's API
    - Make mailman-hyperkitty collect the list archive public key, save it and use it to encrypt when sending to HyperKitty

## References

  - [SELS: A Secure E-mail List Service - [Khurana, Slagell, Bonilla]](https://pdfs.semanticscholar.org/fbc2/f88ccc19eaf864171554e52af66b31bb1e91.pdf)
  - [SELS slides - [Khurana]](http://www.ncsa.illinois.edu/People/hkhurana/UIUCSecurity.pdf)
  - [From Proxy Encryption Primitives to a Deployable Secure-Mailing-List Solution - [Khurana, Heo, Pant]](http://www.ncsa.illinois.edu/People/hkhurana/ICICS.pdf)
  - [Multiplex Encryption: A Practical Approach to Encrypting Multi-Recipient Emails - [Wei, Ding, Chen]](http://www.mysmu.edu/faculty/xhding/publications/m-enc.pdf)
  - [On the Security of a Multi-party Certified Email Protocol - [Zhou]](https://pdfs.semanticscholar.org/faa0/2c9e25afcee3e357a321ca323bfbeddefd9c.pdf)
  - [Schleuder - A gpg-enabled mailinglist with remailing-capabilities.](https://schleuder.nadir.org/)
